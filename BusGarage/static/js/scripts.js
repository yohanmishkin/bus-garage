//$SCRIPT_ROOT = {{ request.script_root|tojson|safe }};

$(function () {

    /************ Index.html ************/
    $('.datepicker').pickadate({
        formatsubmit: "mm/dd/yyyy"
    });

    $('#origin').typeahead({
        source: function (query, process) {
            return $.ajax({
                type: "POST",
                url: "/BusStops",
                data: {query: query},
                dataType: "json",
                success: function (data) {
                    return typeof data.stops == 'undefined' ? false : process(data.stops);
                }
            });
        }
    });

    $('#destination').typeahead({
        source: function (query, process) {
            return $.ajax({
                type: "POST",
                url: "/BusStops",
                data: {query: query},
                dataType: "json",
                success: function (data) {
                    return typeof data.stops == 'undefined' ? false : process(data.stops);
                }
            });
        }
    });


    /************ Rides.html ************/
    if ($('#rides').length > 0) {


        var opts = {
            lines: 15, // The number of lines to draw
            length: 10, // The length of each line
            width: 10, // The line thickness
            radius: 40, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            color: '#888', // #rgb or #rrggbb
            speed: 1, // Rounds per second
            trail: 80, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: true, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
        };
        var target = document.getElementById('rides');
        var spinner = new Spinner(opts).spin(target);

        function spin_stop()
        {
            spinner.stop();
        }
        function spin_start()
        {
            spinner.spin(target);
        }

        var origin = $('.origin').text(),
            destination = $('.destination').text(),
            departure_date = $('.departure_date').text(),
            return_date = $('.return_date').text();

        $.ajax({
            type: "POST",
            url: "/Rides",
            data: {
                origin: origin, destination: destination, departure_date: departure_date, return_date: return_date, driver: 'MegaBus'
            },
            dataType: "json",
            success: function (data) {
                display_results(data, "MegaBus");
                return spin_stop();
            }
        });

//        $.ajax({
//            type: "POST",
//            url: "/Rides",
//            data: {
//                origin: origin, destination: destination, departure_date: departure_date, return_date: return_date, driver: 'Greyhound'
//            },
//            dataType: "json",
//            success: function (data) {
//                display_results(data, "Greyhound");
//                return spin_stop();
//            }
//        });

        function display_results(data, driver) {

            var rides = data['results'];
            var i = 1;

            $.each(rides, function() {
                var id = driver + '-ticket-' + i;
                var ride_result = "<div class='ride-result shadow'><ul>";
                    ride_result += '<li><button id="'
                                + id + '" class="btn-price" data-ticketurl="'
                                + this.ticket_url + '" data-ticketurldata=\''
                                + this.ticket_urldata + '\' onclick="purchaseTicket(\'#'
                                + id + '\')" >'
                                + this.price + '</button></li>' + '<li>'
                                + driver + '</li>'  + '<li title="Departure Time">'
                                + this.departure_datetime + '</li>' + '<li title="Arrival Time">'
                                + this.arrival_datetime + '</li>' + '<li>'
                                + this.duration + '</li>';
                    ride_result += '</ul></div>';

                $('#rides').append(ride_result).children(':last').hide().fadeIn(500);
                i++;
            });
        }

    }

});

function purchaseTicket(id) {
    var url  = $(id).data('ticketurl');
    var urldata = $(id).data('ticketurldata');
    var html = '<form name="ctl01" action="' + url + '" method="POST" id="ctl01" target="_blank" style="display:none">';
    var in_viewstate = '<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="' + urldata.__VIEWSTATE + '">';
    var in_eventvalid = '<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="' + urldata.__EVENTVALIDATION + '">';
    var in_radio = '<input name="JourneyResylts_OutboundList_rbl" id="JourneyResylts_OutboundList_GridViewResults_ctl09_rdoChoose" type="radio" value="' + urldata.JourneyResylts_OutboundList_rbl + '" class="searchSelect">';
    var in_submit = '<input type="submit" name="JourneyResylts$btnAdd" value="Add Journey(s) to basket" id="JourneyResylts_btnAdd" class="addJourneyButton btn">';
    var close_form = '</form>';

    html = html + in_viewstate + in_eventvalid + in_radio + in_submit + close_form;

    $(html).appendTo('#rides');
    $('#JourneyResylts_OutboundList_GridViewResults_ctl09_rdoChoose').attr('checked', 'checked');
    $('#JourneyResylts_btnAdd').trigger('click');

//    $(html).on('submit', function() {
//        $.ajax({
//            type: "POST",
//            url: url,
//            contentType:"application/x-www-form-urlencoded"
//        })
//    }).submit();
}

