from flask import Flask
from BusGarage import config

app = Flask(__name__)
app.config['MONGODB_DB'] = config.MONGODB_DB
app.config['SECRET_KEY'] = config.SECRET_KEY

from BusGarage import views