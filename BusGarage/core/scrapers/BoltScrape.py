import json
import urllib
import urllib2
import unicodedata
import re
from bs4 import BeautifulSoup
from BusGarage.core.models import Ride


class travel_data(object):

    def __init__(self, origin = None, destination = None, departure_date = None, return_date = None):
        self.origin = origin
        self.destination = destination
        self.departure_date = departure_date
        self.return_date = return_date
        self.rides = []


    def get_location_code(self, city):
        url = 'http://us.megabus.com/Default.aspx'
        webpage = urllib2.urlopen(url)

        # figure out what you just fetched
        contentType = webpage.headers['Content-Type']
        ctype, charset = contentType.split(';')

        # get the encoding
        encoding = charset[len(' charset='):]
        # print encoding # ie ISO-8859-1

        utext = webpage.read().decode(encoding) # now you have unicode
        html = utext.encode('utf8', 'ignore')	# encode to uft8

        page = BeautifulSoup(html)

        for child in page.find_all(id='JourneyPlanner_ddlLeavingFrom'):
            for option in child.find_all('option', text=re.compile(city, re.IGNORECASE)):
                return option['value']

        return 'No City Found'

def run(origin, destination, departure_date, return_date):
    travel_request = travel_data(origin, destination, departure_date, return_date)
    return request_data(travel_request)

def request_data(travel_request):
    url = build_url(travel_request)
    return scrape_url(url)

def build_url(travel_request):
    origin = 'originCode=' + travel_request.get_location_code(travel_request.origin)
    destination = 'destinationCode=' + travel_request.get_location_code(travel_request.destination)
    departure_date = 'outboundDepartureDate=' + urllib.quote_plus(travel_request.departure_date)
    return_date = 'inboundDepartureDate=' + urllib.quote_plus(travel_request.return_date)

    url = 'http://us.megabus.com/JourneyResults.aspx?'+\
          origin +\
          '&' + destination +\
          '&' + departure_date +\
          '&' + return_date +\
          '&passengerCount=1'

    return url

def scrape_url(url):
    webpage = urllib2.urlopen(url)

    # figure out what you just fetched
    contentType = webpage.headers['Content-Type']
    ctype, charset = contentType.split(';')

    # get the encoding
    encoding = charset[len(' charset='):]
    utext = webpage.read().decode(encoding)
    html = utext.encode('utf8', 'ignore')	# encode to uft8

    beautifulPage = BeautifulSoup(html)
    return extract_results(beautifulPage)

def extract_results(beautifulPage):
    # json_results = []
    Rides = []
    results = beautifulPage.find_all('ul', 'journey standard')
    for row in results:
    #        ride = {}
        ride = Ride.Ride()
        for element in row.find_all('li', ['two', 'three', 'five']):

            # Departure/Arrival Times
            for time in element.parent.find_all('li', 'two'):
                for depart in time.find_all('p', { 'class' : None }):
                    # Convert to ASCII to we can use Regular Expressions
                    text = unicodedata.normalize('NFKD', depart.get_text()).encode('ascii', 'ignore')
                    arrivalTime = re.search(r'\d{1,2}:\d{2}\s(AM|PM)', text)
                    ride.departure_datetime = arrivalTime.group()
                #                    ride['DepartureTime'] = arrivalTime.group()

                for arrive in time.find_all('p', 'arrive'):
                    # Convert to ASCII to we can use Regular Expressions
                    text = unicodedata.normalize('NFKD', arrive.get_text()).encode('ascii', 'ignore')
                    arrivalTime = re.search(r'\d{1,2}:\d{2}\s(AM|PM)', text)
                    ride.arrival_datetime = arrivalTime.group()
                #                    ride['ArrivalTime'] = arrivalTime.group()

            # Duration
            for duration in element.parent.find_all('li', 'three'):
                ride.duration = duration.get_text().strip()
            #                ride['Duration'] = duration.get_text().strip()

            # Price
            for price in element.parent.find_all('li', 'five'):
                ride.price = price.get_text().strip()
            #                ride['Price'] = price.get_text().strip()

            ride.ticket_url = 'http://us.megabus.com/ViewBasket.aspx'

        # json_results.append(ride)
        Rides.append(ride)

    # return json_results
    return Rides


def format_results(results):
    data = {'MegaBusResults':results}
    print json.dumps(data)
    return json.dumps(data)
