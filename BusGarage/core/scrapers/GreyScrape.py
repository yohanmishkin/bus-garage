import json
import requests
import re
import operator
from operator import attrgetter
from BusGarage.core.models import Ride

class travel_data(object):

    def __init__(self, origin = None, destination = None, departure_date = None, return_date = None):
        self.origin = origin
        self.destination = destination
        self.departure_date = departure_date
        self.return_date = return_date
        self.rides = []

    def get_location_code(self, city):
        url = 'https://www.greyhound.com/services/locations.asmx/GetDestinationLocationsByName'
        headers = {'content-type': 'application/json'}
        payload = {"context":{"Text": city,"NumberOfItems":0}}
        request = requests.post(url, data=json.dumps(payload), headers=headers)
        location_code = request.json()['d']['Items'][0]['Value']
        return location_code


def run(origin, destination, departure_date, return_date):
    travel_request = travel_data(origin, destination, departure_date, return_date)
    return request_data(travel_request)

def request_data(travel_request):
    payload = build_payload(travel_request)
    return scrape_url(payload)


def build_payload(travel_request):
    payload = {"__type":"Greyhound.Website.DataObjects.ClientSearchRequest","Mode":2,"Adults":1}
    payload['Origin'] = travel_request.get_location_code(travel_request.origin)				# "Origin":"151239|New York/NY"
    payload['Destination'] = travel_request.get_location_code(travel_request.destination)	# "Destination":"40030|Boston/MA"
    payload['Departs'] = format_date(travel_request.departure_date)							# "Departs":"08 November 2012"
    payload['Returns'] = format_date(travel_request.return_date) 							# "Returns":None

    request_payload = {'request':payload}
    return request_payload

def format_date(input):
    return input.replace(',', '')

def scrape_url(payload):
    url = 'https://www.greyhound.com/services/farefinder.asmx/Search'
    headers = {'content-type': 'application/json'}
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    results = parse_json(response.json(), payload)
    return results

def parse_json(data, payload):
    Rides = []
    for listing in data['d']['SchedulesDepart']:
        departure_datetime_raw = listing['DisplayDeparts'].replace('<br/>', ' ')
        departure_datetime = re.match('(?P<depature_time>(0[1-9]|1[0-2]):[0-5][0-9][ ][AP]M)', departure_datetime_raw).group('depature_time')
        arrival_datetime_raw = listing['DisplayArrives'].replace('<br/>', ' ')
        arrival_datetime = re.match('(?P<arrival_time>(0[1-9]|1[0-2]):[0-5][0-9][ ][AP]M)', arrival_datetime_raw).group('arrival_time')
        duration = listing['Time']
        fare = determine_lowest_price(listing)
        price = fare['Price']
        ticket_url = create_ticket_url(fare['Key'])
        ticket_urldata = json.dumps(payload)
        #TODO Store Key in ride object (as ticket_url?)
        #payload = '{"request":{"__type":"Greyhound.Website.DataObjects.ClientSearchRequest","Mode":0,"Origin":"151239|New York/NY","Destination":"150051|Albany/NY","Departs":"24 March 2013","Returns":null,"TimeDeparts":null,"TimeReturns":null,"RT":false,"Adults":1,"Seniors":0,"Children":0,"PromoCode":"","DiscountCode":"","Card":"","CardExpiration":"03/2013"}}'

        ride = Ride.Ride(duration, departure_datetime, arrival_datetime, price, ticket_url, ticket_urldata.decode('unicode-escape'))
        Rides.append(ride)

    return Rides

def determine_lowest_price(listing):
    lowest_fare = None
    fares = [listing['Fare1'], listing['Fare2'], listing['Fare3'], listing['Fare4']]

    for fare in fares:
        if fare['Available'] == True:
            if lowest_fare == None or fare['Total'] < lowest_fare['Price']:
                lowest_fare = { 'Price': fare['Total'], 'Key': fare['Key'] }

    return lowest_fare


def create_ticket_url(key):
    url = 'https://www.greyhound.com/services/farefinder.asmx/Search'
    return url


def format_results(results):
    data = {'GreyhoundResults':results}
    print json.dumps(data)
    return json.dumps(data)
