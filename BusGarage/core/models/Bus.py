import json

class Bus(object):

    driver = None
    rides = []

    def __init__(self, driver):
        self.driver = driver

    def get_stops(self):
        return ['']

    def get_rides(self, origin, destination, departure_date, return_date):

        if not self.rides:

            if self.driver == 'MegaBus':
                from BusGarage.core.scrapers.MegaScrape import run
                self.rides = run(origin, destination, departure_date, return_date)
                return self.rides

            elif self.driver == 'Greyhound':
                from BusGarage.core.scrapers.GreyScrape import run
                self.rides = run(origin, destination, departure_date, return_date)
                return self.rides

            elif self.driver == 'Bolt':
                from BusGarage.core.scrapers.BoltScrape import run
                self.rides = run(origin, destination, departure_date, return_date)
                return self.rides

            elif self.driver == 'China':
                from BusGarage.core.scrapers.ChinaScrape import run
                self.rides = run(origin, destination, departure_date, return_date)
                return self.rides

        return self.rides

    def get_json_rides(self):
        json_rides = []
        for ride in self.rides:
            json_ride = {}
            json_ride['arrival_datetime'] = ride.arrival_datetime
            json_ride['departure_datetime'] = ride.departure_datetime
            json_ride['duration'] = ride.duration
            json_ride['price'] = ride.price
            json_ride['ticket_url'] = ride.ticket_url
            json_ride['ticket_urldata'] = ride.ticket_urldata

            json_rides.append(json_ride)

        return json_rides