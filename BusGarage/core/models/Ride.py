
class Ride(object):

    duration = None
    departure_datetime = None
    arrival_datetime = None
    price = 0
    ticket_url = None
    ticket_urldata = None

    def __init__(self, duration = None, departure_datetime = None, arrival_datetime = None, price = 0, ticket_url = None, ticket_urldata = None):
        self.duration = duration
        self.departure_datetime = departure_datetime
        self.arrival_datetime = arrival_datetime
        self.price = price
        self.ticket_url = ticket_url
        self.ticket_urldata = ticket_urldata